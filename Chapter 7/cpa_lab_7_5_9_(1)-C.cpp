#include <iostream>
#include <sstream>
#include <regex>
#include <cstring>

using namespace std;

class ip_header
{
private:
    int sourse[4], destination[4];
    regex e;

public:
    ip_header(string s = "0.0.0.0", string t = "0.0.0.0.")
    {
        e = string("^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$");
        if (!regex_match(s, e))
        {
            throw string("source ip hasn't correct format");
        }
        if (!regex_match(t, e))
        {
            throw string("destination ip hasn't correct format");
        }

        int position = s.find(".");
        sourse[0] = atoi(s.substr(0, position).c_str());
        destination[0] = atoi(s.substr(0, position).c_str());

        sourse[1] = atoi(s.substr(position + 1, s.find(".", position + 1)).c_str());
        destination[1] = atoi(s.substr(position + 1, s.find(".", position + 1)).c_str());

        position = s.find(".", position + 1);
        sourse[2] = atoi(s.substr(position + 1, s.find(".", position + 1)).c_str());
        destination[2] = atoi(s.substr(position + 1, s.find(".", position + 1)).c_str());

        position = s.find(".", position + 1);
        sourse[3] = atoi(s.substr(position+1).c_str());
        destination[3] = atoi(s.substr(position + 1).c_str());
        if (sourse[0] > 255 || sourse[1] > 255 || sourse[2] > 255 || sourse[3] > 255)
        {
            throw string("source ip: incorrect range");
        }
        if (destination[0] > 255 || destination[1] > 255 || destination[2] > 255 || destination[3] > 255)
        {
            throw string("destination ip: incorrect range");
        }
    }
};

int main()
{
	string ip_addr_1, ip_addr_2;
	stringstream ss;
	getline(cin, ip_addr_2);
	ss << ip_addr_2;
	ss >> ip_addr_1;
	ss >> ip_addr_2;
	try
    {
        ip_header ip(ip_addr_1, ip_addr_2); cout << "valid ip header" << endl;
    }
	catch (string e)
    {
        cout << e << endl;
        return 1;
    }

	ss.clear();
	getline(cin, ip_addr_2);
	ss << ip_addr_2;
	ss >> ip_addr_1;
	ss >> ip_addr_2;
	try
    {
        ip_header ip2(ip_addr_1, ip_addr_2); cout << "valid ip header" << endl;
    }
	catch (string e)
    {
        cout << e << endl;
        return 1;
    }
    return 0;
}



