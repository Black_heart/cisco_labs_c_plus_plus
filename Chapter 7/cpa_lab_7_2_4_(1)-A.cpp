#include <iostream>

using namespace std;

struct argument_invalid : exception 
{
    const char* what() const noexcept
    {
        return "Argument is less than zero.";
    }
};

float square_area(float x)
{
    if(x <= 0)
        throw argument_invalid();
    return x * x;
}

float rectangle_area(float a, float b)
{
    if(a <= 0 || b <= 0)
        throw argument_invalid();
    return a * b;
}

int main(void)
{
    float a, b, r;
    cin >> a;
    cin >> b;
    try
    {
        float rsquare = square_area(a);     
        float rrectangle = rectangle_area(a,b);
        cout << rsquare << endl << rrectangle << endl;
    }
    catch(const exception& e)
    {
        cerr << e.what() << endl;
    }
    return 0;
}