#include <iostream>

using namespace std;

float div_with_params_validation(float arg1, float arg2) {
    if(arg2 == 0.0)
        throw runtime_error("Bad argument exception.");
    return arg1 / arg2;
}

float div_with_catch_block(float arg1, float arg2) {
    try
    {
        return arg1 / arg2;
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }    
}

int main(void) {
    float r, a, b;
    cout << "Method with params validation" << endl << "Enter a, b: ";
    cin >> a >> b;
    try
    {
        r = div_with_params_validation(a,b);
        cout << r << endl;
    }
    catch(const exception& e)
    {
        cerr << e.what() << endl;
    }

    cout << "Method with catch block" << endl << "Enter a, b: ";
    cin >> a >> b;
    r = div_with_catch_block(a,b);
    cout << r << endl;
    return 0;
}