#include <iostream>
#include <vector>

using namespace std;

class caretaker
{
    private:
        int value, minvalue, maxvalue;
    public:
        caretaker(int val, int minval, int maxval)
        {
            value = val;
            minvalue = minval;
            maxvalue = maxval;
        }
        void add(int a)
        {
            int temp = value + a;
            if (temp > maxvalue || temp < minvalue) { throw "Value could exceed limit"; }
            else value = temp;
        }
        void substruct(int a)
        {
            add(-a);
        }
        int get_value()
        {
            return value;
        }
};

int main()
{
	int a, mina, maxa;
	vector<caretaker> caretakers;
	for(int i = 0; i < 2; ++i){
        cout << "enter value number " << i << endl;
        cin >> a;
        cout << "enter min value number " << i << endl;
        cin >> mina;
        cout << "enter max value number " << i << endl;
        cin >> maxa;

        caretakers.push_back(caretaker(a, mina, maxa));
	}

	cout << "enter value to add" << endl;
	cin >> a;

    for(int i = 0; i < 2; ++i)
        try
        {
            caretakers[i].add(a);
            cout << "Value number " << i << " + " << a << " = " << caretakers[i].get_value() << endl;
        }
        catch (...)
        {
            cout << "Value number " << i << " could exceed limit" << endl;
        }

	cout << "enter value to substruct" << endl;
	cin >> a;

	for(int i = 0; i < 2; ++i)
        try
        {
            caretakers[i].substruct(a);
            cout << "Value number " << i << " - " << a << " = " << caretakers[i].get_value() << endl;
        }
        catch (...)
        {
            cout << "Value number " << i << " could exceed limit" << endl;
        }

    return 0;
}
