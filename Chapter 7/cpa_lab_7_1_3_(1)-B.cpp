#include <iostream>

int main(void) {
    using namespace std;
    int a = 0, b = 0, c = 0;
    cout << "Enter a nad b: ";    
    cin >> a >> b;
    try
    {
        b == 0 ? throw runtime_error("Your input is not valid, you can't divide by zero.")
               : c = a / b; 
        cout << c << endl;
    }
    catch(const exception& e)
    {
        cerr << e.what() << '\n';
        return 1;
    }
    return 0;
}