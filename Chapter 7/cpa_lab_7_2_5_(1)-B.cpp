#include <iostream>

using namespace std;

class my_exception : exception 
{
public:
    const char* what() const noexcept{
        return message;
    }
private:
    const char* message {"Invalid argument"};
};


template<typename T>
class matrix
{
public:

    matrix(size_t m, size_t n) : _m(m), _n(n){
        if((m <= 0) && (n <= 0))
            throw my_exception();
        value = new T[m * n];
    }

    ~matrix(){
        if (value != nullptr)
            delete[] value;
    }

    size_t m(){
        return _m;
    }

    size_t n(){
        return _n;
    }

    void set(size_t m, size_t n, T v){
        value[m * _n + n] = v;
    }

    T get(size_t m, size_t n){
        return value[m * _n + n]; 
    }

    void add_matrix(matrix& m){
        if(m.m() != _m || m.n() != _n)
            throw my_exception();
        for(size_t i = 0; i < _m; ++i){
            for(size_t j = 0; j < _n; ++j){
                value[i * _n + j] += m.get(i, j);
            }
        }                
    }

    void add_item(T item){
        for(size_t i = 0; i < _m; ++i){
            for(size_t j = 0; j < _n; ++j){
                value[i * _n + j] += item;
            }
        }     
    }

    void print(){
        for(int i = 0; i < _m; ++i){
            for(int j = 0; j < _n; ++j){
                cout << value[i * _n + j] << " ";
            }
            cout << endl;
        }
        cout << endl;
    }

private:
    size_t _m;
    size_t _n;
    T* value = nullptr;
};

int main(void)
{
    matrix<int> m(2, 3);
    for(int i = 0; i < 2; ++i){
        for(int j = 0; j < 3; ++j){
            m.set(i, j, 2);
        }
    }

    m.print();
    m.add_item(6);
    m.print();

    matrix<int> k(3, 2);
    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 2; ++j){
            k.set(i, j, 3);
        }
    }

    matrix<int> n(3, 2);
    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 2; ++j){
            n.set(i, j, 3);
        }
    }

    n.print();    
    n.add_matrix(k);
    n.print();

    try{
        n.add_matrix(m);
        n.print();
    }
    catch(const my_exception& e)
    {
        cerr << e.what() << endl;
        return 1;
    } 
    catch(const exception& e)
    {
        cerr << e.what() << endl;
        return 2;
    }      
    return 0;
}