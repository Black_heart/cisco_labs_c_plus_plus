#include <iostream>
#include <fstream>
#include <stdexcept>
#include <sys/stat.h>

using namespace std;

class matrix{
    private:
        int matrix[2][2];
    public:
        int get_matrix_element(int arrayIndex, int elementIndex);
        void load_matrix(string path);
        void save_matrix(string path);
};

bool is_file_exists (string path) {
  struct stat buffer;
  return (stat (path.c_str(), &buffer) == 0);
}

void matrix::load_matrix(string path){
    if(!is_file_exists(path))
        throw runtime_error("File not found at: " + path);

    ifstream iFile (path);

    if(!iFile)
        throw runtime_error("No rights to read from file: " + path);

    iFile >> matrix[0][0] >> matrix[0][1] >> matrix[1][0] >> matrix[1][1];

    iFile.close();
}

void matrix::save_matrix(string path){
    if(!is_file_exists(path))
        throw runtime_error("File not found at: " + path);

    ofstream oFile (path);

    if(!oFile){
        throw runtime_error("No rights to write to file: " + path);
    }

    oFile << matrix[0][0] << " " << matrix[0][1] << endl << matrix[1][0] << " " << matrix[1][1];

    oFile.close();
}

int matrix::get_matrix_element(int arrayIndex, int elementIndex){
    if(arrayIndex < 0 || arrayIndex >= 2 || elementIndex < 0 || elementIndex >= 2)
        throw invalid_argument("Index out of range");

    return this->matrix[arrayIndex][elementIndex];
}

int main()
{
    string path;
    matrix m;

    cout << "Enter path to file for read matrix:" << endl;
    getline(cin, path);

    try{
        m.load_matrix(path);

        cout << "Matrix:" << endl << m.get_matrix_element(0, 0) << " " << m.get_matrix_element(0, 1) <<
            endl << m.get_matrix_element(1, 0) << " " << m.get_matrix_element(1, 1) << endl;

        cout << "Enter path to file for save matrix:" << endl;
        getline(cin, path);

        m.save_matrix(path);
    }
    catch (const exception& e){
        cout << e.what() << endl;
    }

    return 0;
}
