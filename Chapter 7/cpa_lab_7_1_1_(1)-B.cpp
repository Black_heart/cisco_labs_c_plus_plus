#include <iostream>

int main(void) {
    using namespace std;
    int a = 8, b = 0, c = 0;

    cout << "b: ";
    cin >> b;
    try
    {
        b == 0 ? throw runtime_error("Your input is not valid, you can't divide by zero.")
               : c = a / b;
    }
    catch(const exception& e)
    {
        cerr << e.what() << endl;
        return 1;
    }    
    
    cout << c << endl;
    return 0;
}