#include <iostream>

using namespace std;

enum weekday
{
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY
};

enum month
{
    january = 1,
    february,
    march,
    april,
    may,
    june,
    july,
    august,
    september,
    oktober,
    november,
    december
};

class date
{
public:
    int _day, _month, _year;

    date(int d = 1, int m = 1, int y = 1970)
    {
        if (y < 1970)
            throw string("data must be more 1.1.1970");
        if(m < 1 || m > 12)
            throw string("month in range [1....12]");
        if (d < 1 || d > 31)
            throw string("day in range [1....31]");
        _day = d;
        _month = m;
        _year = y;
    }

    string day_week (int da) {
        switch (da) {
            case MONDAY:    return"Monday";
            case TUESDAY:   return "Tuesday";
            case WEDNESDAY: return "Wednesday";
            case THURSDAY:  return "Thursday";
            case FRIDAY:    return "Friday";
            case SATURDAY:  return"Saturday";
            case SUNDAY:    return "Sunday";
            default:        return "Somewhere inside the depths of time...";
        }
        return "";
    }

    string month(int da)
    {
        switch (da) {
            case january:    return "january";
            case february:   return "february";
            case march:      return "march";
            case april:      return "april";
            case may:        return "may";
            case june:       return "june";
            case july:       return "july";
            case august:     return "august";
            case september:  return "september";
            case oktober:    return "oktober";
            case november:   return "november";
            case december:   return "december";
            default:         return "Somewhere inside the depths of time...";
        }
        return "";
    }

    bool is_leap(int year)
    {
        return year % 400 == 0 || year % 100 != 0 && year % 4 == 0;
    }

    int month_length(int year, int month)
    {
        if (month == 2 && is_leap(year))
            return 29;

        int lengths[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        return lengths[month - 1];
    }
    
    int day_of_year(date date)
    {
        int res = date._day;
        for (int i = 1; i < date._month; i++)
            res += month_length(date._year, i);
        return res;
    }

    int days_between(date d1, date d2)
    {
        if(d1._year > d2._year)
            return -1;

        int daysCount = day_of_year(d2) - day_of_year(d1);
        if(d1._year == d2._year && daysCount < 0)
            return -1;

        for(int i = d1._year; i < d2._year; ++i)
            daysCount += is_leap(i) ? 366 : 365;
        return daysCount;
    }

    int operator-(date w)
    {
        return days_between(w, *this);
    }
};

int main()
{
    int day, month, year;
    cout << "Enter day: ";
    cin >> day;
    cout << "Enter month: ";
    cin >> month;
    cout << "Enter year: ";
    cin >> year;
	date q(day, month, year);
	date w(1, 1, 1970);
	int qq = q - w - 1;
	cout << q._day << " " << q.month(q._month) << " " << q._year << " - " << q.day_week(qq % 7) <<
        " - "<< qq <<" days since 1st January 1970" << endl;
    return 0;
}
