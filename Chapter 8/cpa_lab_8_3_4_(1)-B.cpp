#include <iostream>
#include <set>
#include <sstream>
#include <stdlib.h>

using namespace std;

string str(int value)
{
    stringstream ss;
    ss << value;
    return ss.str();
}

struct node
{

	int value;
	node* leftChild;
	node* rightChild;
	node() : node(0) { };
	node(int value)
	{
		this->value = value;
		leftChild = nullptr;
		rightChild = nullptr;
	}
	string Print()
	{
		string s = "";
		if (leftChild != nullptr) s += leftChild->Print();
		s.append(str(value));
		s.append("\n");
		if (rightChild != nullptr) s += rightChild->Print();
		return s;
	}
};

struct binary_tree
{
public:
	node root;
	void add(int value);
	bool find(int value);

	binary_tree(int value)
	{
		root = node(value);
	}

};

bool binary_tree::find(int value)
{
	node* temp = &this->root;
	while (temp != nullptr && temp->value != value)
	{
		if (temp->value > value) temp = temp->leftChild;
		else temp = temp->rightChild;
	}
	return temp != nullptr && temp->value == value;
}

void binary_tree::add(int value)
{
	if (!find(value))
	{
		node* temp = &this->root;
		while (temp->value != value)
		{
			if (temp->value > value)
			{
				if (temp->leftChild != nullptr)
					temp = temp->leftChild;
				else break;
			}
			else
			{
				if (temp->rightChild != nullptr)
					temp = temp->rightChild;
				else break;
			}
		}

		if (temp->value > value)
			temp->leftChild = new node(value);
		else
			temp->rightChild = new node(value);

	}
}

ostream& operator<<(ostream& os, binary_tree& tree)
{
	os << tree.root.Print();
	return os;
}

int main()
{
	cout << "Input nodes (to stop input 'z'):" << endl;

	int node;
	string line;
	cin >> line;
	binary_tree tree(atoi(line.c_str()));
	do
	{
		cin >> line;
		if (line == "z")
            break;
		node = atoi(line.c_str());
		tree.add(node);
	}
    while (true);
	cout << tree << endl;

    return 0;
}
