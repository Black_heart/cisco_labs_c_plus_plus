#include <iostream>

using namespace std;

class stack
{
private:
    int *stackstore;
    int stacksize;
    int SP;
public:
    stack(int size) 
    {
        if(size <= 0)
            throw string("size must be >= 0");
        try
        {
            stackstore = new int[size];
        }
        catch(bad_alloc ba)
        {
            throw string("stack_bad_alloc");
        }
        stacksize = size;
        SP = 0;
    }
    ~stack(void)
    {
        delete stackstore;
    }
    void push(int value)
    {
        if(SP == stacksize)
            throw string("stack size exceeded");
        stackstore[SP++] = value;
    }
    int pop(void)
    {
        if(SP == 0)
            throw string("stack is empty");
        return stackstore[--SP];
    }
    int top(void)
    {
        if(SP == 0)
            throw string("stack is empty");
        return stackstore[SP];
    }
    stack& operator<< (int v)
    {
        push(v);
        return *this;
    }
    stack& operator>>(int &v)
    {
        v = pop();
        return *this;
    }
};

int main(void)
{
    int n;

    cout << "Enter how many items should be pushed onto the stack: ";
    cin >> n;
    stack stk(n);
    int j;
    try
    {
        for (int i = 0; i < n; i++)
        {
            cin >> j;
            stk << j;
        }
    }
    catch(string e)
    {
        cout << "Exeption: " << e;
    }

    cout << "Enter how many items your program should print: ";
    cin >> n;
    try
    {
        for (int i = 0; i < n; i++)
        {
            stk >> j;
            cout << j << endl;
        }
    }
    catch(string e)
    {
        cout << "Exeption: " << e;
    }
    return 0;
}
