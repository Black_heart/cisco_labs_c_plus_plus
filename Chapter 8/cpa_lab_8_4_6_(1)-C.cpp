#include <iostream>

using namespace std;

class binary_tree
{
public:
    binary_tree()
    {
        root = NULL;
    }

    void insert(int x)
    {
        root = add(x, root);
    }

    void display()
    {
        print(root);
    }

    int operator[] (int index)
    {
        if (index < 1)
            throw string("index out of range");
        int value;
        find_by_index(root, index, value);
        return value;
    }

protected:
    struct node
    {
        int data;
        node* left;
        node* right;
    };
    node* root;
    
    node* add(int x, node* t)
    {
        if(t == NULL)
        {
            t = new node;
            t->data = x;
            t->left = t->right = NULL;
        }
        else if(x < t->data)
            t->left = add(x, t->left);
        else if(x > t->data)
            t->right = add(x, t->right);
        return t;
    }

    virtual void print(node* t){ }
    virtual void find_by_index(node* t, int &index, int &value){ }
};

class inorder_tree: public binary_tree
{
    void print(node* t)
    {
        if(t == NULL)
            return;
        print(t->left);
        cout << t->data << " ";
        print(t->right);
    }
    void find_by_index(node* t, int& index, int& value)
    {
        if(t == NULL)
            return;
        find_by_index(t->left,index,value);
        if(index == 0)
            value = t->data;
        index--;
        find_by_index(t->right,index,value);
    }
};

class postorder_tree: public binary_tree
{
    void print(node* t)
    {
        if(t == NULL)
            return;
        print(t->left);
        print(t->right);
        cout << t->data << " ";
    }
    void find_by_index(node* t, int& index, int& value)
    {
        if(t == NULL)
            return;
        find_by_index(t->left,index,value);

        find_by_index(t->right,index,value);
        if(index == 0)
            value = t->data;
        index--;
    }
};

class preorder_tree: public binary_tree
{
    void print(node* t)
    {
        if(t == NULL)
            return;
        cout << t->data << " ";
        print(t->left);
        print(t->right);
    }
    void find_by_index(node* t, int& index, int& value)
    {
        if(t == NULL)
            return;
        if(index == 0)
            value = t->data;
        index--;
        find_by_index(t->left,index,value);

        find_by_index(t->right,index,value);
    }
};

void operator<< (ostream &strm, inorder_tree t) {
    t.display();
}

void operator<< (ostream &strm, postorder_tree t) {
    t.display();
}

void operator<< (ostream &strm, preorder_tree t) {
    t.display();
}

int main()
{
    int n;
    cout << "Enter how many items should be pushed onto the tree: ";
    cin >> n;
    inorder_tree it;
    postorder_tree pot;
    preorder_tree prt;
    int j;
    for(int i = 0; i < n; i++)
    {
        cin >> j;
        it.insert(j);
        pot.insert(j);
        prt.insert(j);
    }

    cout << "Enter number of item: ";
    cin >> j;
    cout << "Inorder: "<< it[j];
    cout << endl;
    cout << "Postorder: "<< pot[j];
    cout << endl;
    cout << "Preorder: "<< prt[j];
    cout << endl;
    cout << "Inorder: "<< it;
    cout << endl;
    cout << "Postorder: "<< pot;
    cout << endl;
    cout << "Preorder: "<< prt;
    cout << endl;


    return 0;
}
