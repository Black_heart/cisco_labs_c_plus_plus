#include <iostream>
#include <stdexcept>

using namespace std;

class matrix
{
private:
    int32_t m[2][2];
    int array_index;
    int element_index;
    int array_size;
public:
    matrix() : array_size(2), array_index(0), element_index(0) {}

    friend istream& operator>> (istream& is, matrix& matrix);
    friend ostream& operator<< (ostream& os, matrix& matrix);
};

istream& operator>> (istream& is, matrix& matrix)
{
    if(matrix.array_index == matrix.array_size && matrix.element_index == matrix.array_size)
        throw invalid_argument("matrix is full");

    if(matrix.element_index == matrix.array_size)
    {
        matrix.element_index = 0;
        ++matrix.array_index;
    }

    is >> matrix.m[matrix.array_index][matrix.element_index];
    ++matrix.element_index;

    return is;
}

ostream& operator<< (ostream& os, matrix& matrix)
{
    os << matrix.m[0][0] << " " << matrix.m[0][1] << endl <<
        matrix.m[1][0] << " " << matrix.m[1][1];
    return os;
}

int main()
{
    matrix m = matrix();
    for(size_t i = 0; i < 2; ++i)
    {
        for(size_t j = 0; j < 2; ++j)
        {
            cout << "Element[" << i << "][" << j <<"]: ";
            cin >> m;
        }
    }
    cout << "Matrix:" << endl << m;
    return 0;
}
