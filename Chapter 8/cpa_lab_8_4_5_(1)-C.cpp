#include <iostream>
#include <vector>
#include <stdexcept>

using namespace std;

enum state { start = 1, second, third, fourth, fifth, stop };

class fsm
{
public:
    fsm()
    {
        currentState = start;
        visitedStates.push_back(start);
    }
    state current_state();
    friend istream& operator>> (istream& is, fsm& fsm);
    friend ostream& operator<< (ostream& os, fsm& fsm);
private:
    state currentState;
    vector<state> visitedStates;
};

state fsm::current_state()
{
    return this->currentState;
}

istream& operator>> (istream& is, fsm& fsm)
{
    int value;
    is >> value;

    switch(fsm.currentState){
        case start:
            if(value < 5)
                fsm.currentState = third;
            else fsm.currentState = second;
            break;
        case second:
            if(value < 5)
                fsm.currentState = third;
            else fsm.currentState = fifth;
            break;
        case third:
            if(value < 5)
                fsm.currentState = second;
            else fsm.currentState = fourth;
            break;
        case fourth:
            if(value < 5)
                fsm.currentState = stop;
            else fsm.currentState = fifth;
            break;
        case fifth:
            if(value < 5)
                fsm.currentState = stop;
            else fsm.currentState = fourth;
            break;
        case stop:
            throw runtime_error("fsm is on stop state!");
            break;
    }
    fsm.visitedStates.push_back(fsm.currentState);

    return is;
}

ostream& operator<< (ostream& os, fsm& fsm){
    os << "States visited:" << endl;

    for(int i = 0; i < fsm.visitedStates.size() - 1; ++i)
        os << fsm.visitedStates[i] << (fsm.visitedStates[i] == start ? "(start)" : "") << ", ";

    os << fsm.visitedStates.back() << (fsm.visitedStates.back() == stop ? "(stop)" : "") << endl;

    return os;
}

int main()
{
    fsm instance = fsm();
    while(instance.current_state() != stop)
    {
        cout << "Current: " << instance.current_state() << endl << "Change: ";
        cin >> instance;
        cout << endl;
    }
    cout << "stop state reached" << endl << instance;
    return 0;
}
