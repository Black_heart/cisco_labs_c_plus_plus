#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int first, second;

    cout << "Enter first: ";
    cin >> first;
    cout << "Enter second: ";
    cin >> second;

    double first = 1.0 / first, second = 1.0 / second;
    cout << (abs(first-second) < 0.000001 ?
             "Results are equal (by 0.000001 epsilon)" : "Results are not equal (by 0.000001 epsilon)") << endl;
             
    return 0;
}



   