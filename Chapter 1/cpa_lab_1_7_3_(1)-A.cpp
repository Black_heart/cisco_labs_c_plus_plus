#include <iostream>

using namespace std;

int main()
{
    double a, b, c, d, e;

    cout << "Input values:" << endl;
    cin >> a >> b >> c >> d >> e;

    cout << "Output values:" << endl << fixed;
    cout.precision(1);
    cout << a << endl;

    cout.precision(2);
    cout << b << endl;

    cout.precision(6);
    cout << c << endl;

    cout.precision(2);
    cout << d << endl;

    cout.precision(0);
    cout << e;

    return 0;
}


   
