#include <iostream>

using namespace std;

bool is_in_range(int number)
{
    return number > 0 && number < 256;
}

int main()
{
    int a, b, c, d;
    cin >> a >> b >> c >> d;
    if(is_in_range(a) && is_in_range(b) && is_in_range(c) && is_in_range(d))
        cout << a << "." << b << "." << c << "." << d << endl;
    else throw std::exception();
    return 0;
}


   
