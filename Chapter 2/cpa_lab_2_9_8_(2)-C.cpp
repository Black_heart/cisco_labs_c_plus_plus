﻿#include <iostream>

using namespace std;

int main()
{
	int banknotes[] = { 50, 20, 10, 5, 1 };
	int money;

	cout << "Enter needed money: ";
	cin >> money;

	for (size_t i = 0; money > 0;)
	{
		if (money >= banknotes[i])
		{
			money -= banknotes[i];
			cout << banknotes[i] << ' ';
		}
		else
		{
			++i;
		}
	}

	cout << endl;
	return 0;
}


  