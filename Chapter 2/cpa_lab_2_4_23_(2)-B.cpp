﻿#include <iostream>

using namespace std;

int main()
{
	unsigned short int val, number = 0;
	unsigned short int n = 32768;
	
	cout << "value";
	cin >> val;
	for (int index = 0; index < 16; ++index)
	{
		if (n & val)
			number |= 32768 / n;
		n >>= 1;
	}
	if (number == val)
	{
		cout << val << " is a bitwise palindrome" << endl;
	}
	else
	{
		cout << val << " is not a bitwise palindrome" << endl;
	}
	return 0;
}

  
