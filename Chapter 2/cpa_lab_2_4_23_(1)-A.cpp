﻿#include <iostream>

using namespace std;

int main()
{
	unsigned long c = 0;
	int n;
	cout << "Enter a n: ";
	cin >> n;

	while (n)
	{
		c += n & 1;
		n >>= 1;
	}
	cout << c;
	return 0;
}

  