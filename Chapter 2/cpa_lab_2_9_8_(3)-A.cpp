﻿#include <iostream>

using namespace std;

int main()
{
	int vector[] = { 5, 1, 7, 3, 8, 3, 7, 1 };
	bool is_palindrome = true;
	int n = sizeof(vector) / sizeof(vector[0]);

	for (int i = 0; i < n / 2; ++i)
	{
		if (vector[i] != vector[n - 1 - i])
		{
			is_palindrome = false;
			break;
		}
	}

	if (is_palindrome)
	{
		cout << "It's a palindrome";
	}
	else
	{
		cout << "It isn't a palindrome";
	}
	cout << endl;
	return 0;
}

   
