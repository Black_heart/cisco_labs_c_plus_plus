﻿#include <iostream>

using namespace std;

int main()
{
	double matrix[][5] = {
		{ 1, 2, 3, 4, 4 },
		{ 2, 2, 3, 1, 4 },
		{ 3, 3, 3, 2, 4 },
		{ 4, 1, 2, 4, 4 },
		{ 1, 1, 1, 1, 4 } };
	int side = sizeof(matrix[0]) / sizeof(matrix[0][0]);
	bool is_symmetric = true;
	int size = sizeof(matrix) / sizeof(*matrix);

	is_symmetric = side == size;

	for (int i = 0; is_symmetric && i < side; ++i)
	{
		for (int j = i + 1; is_symmetric && j < side; ++j)
		{
			is_symmetric = matrix[i][j] == matrix[j][i];
		}
	}

	if (is_symmetric)
	{
		cout << "The matrix is symmetric" << endl;
	}
	else
	{
		cout << "The matrix is not symmetric" << endl;
	}
	return 0;
}

  
