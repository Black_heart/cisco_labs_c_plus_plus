﻿#include <iostream>

using namespace std;

int main()
{
	int fib;
	cin >> fib;
	long long first_number = 1, second_number = 1, third_number = 1;

	for (size_t i = 0; i < fib - 2; ++i)
	{
		third_number = first_number + second_number;
		first_number = second_number;
		second_number = third_number;
	}

	cout << third_number << endl;
	return 0;
}
