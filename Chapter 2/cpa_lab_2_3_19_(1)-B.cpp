﻿#include <iostream>

using namespace std;

int main()
{
	int c, i = 0;
	cout << "Input:" << endl;
	cin >> c;
	cout << "Output:" << endl;
	while (c != 1)
	{
		++i;
		if (c % 2 == 0)
			c /= 2;
		else c = 3 * c + 1;
		cout << c << endl;
	}
	cout << "steps = " << i;
	return 0;
}


   
      
