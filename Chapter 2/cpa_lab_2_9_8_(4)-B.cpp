﻿#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	double vector[] = { 1., 2., 3., 4., 5. };
	int n = sizeof(vector) / sizeof(vector[0]);
	double arithmetic_mean = 0, harmonic_mean = 0, geometric_mean = 1, root_mean_square = 0;

	for (int i = 0; i < n; i++)
	{
		arithmetic_mean += vector[i];
		harmonic_mean += 1. / vector[i];
		geometric_mean *= vector[i];
		root_mean_square += vector[i] * vector[i];
	
	}

	arithmetic_mean /= n;
	harmonic_mean = n / harmonic_mean;
	geometric_mean = pow(geometric_mean, 1. / n);
	root_mean_square = pow(root_mean_square / n, 1. / 2);


	cout << "Arithmetic mean = " << arithmetic_mean << endl
	     << "Harmonic mean = " << harmonic_mean << endl
	     << "Geometric mean = " << geometric_mean << endl
	     << "Root mean square = " << root_mean_square << endl;

	return 0;
}
