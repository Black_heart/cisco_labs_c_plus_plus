﻿#include <iostream>

using namespace std;

int main()
{
	int c;
	double one, two;

	while (true)
	{
		cout << "MENU:" << endl << "0 - exit" << endl << "1 - addition" << endl << "2 - subtraction" << endl;
		cout << "3 - multiplication" << endl << "4 - division" << endl << "Your choice?" << endl;
		cin >> c;
		if (c == 0)
			break;
		cout << "Enter One number: ";
		cin >> one;
		cout << "Enter Two Number: ";
		cin >> two;
		switch (c)
		{
		case 1:
			cout << "Result a + b = " << one + two << endl << endl;
			break;
		case 2:
			cout << "Result a - b = " << one - two << endl << endl;
			break;
		case 3:
			cout << "Result a * b = " << one * two << endl << endl;
			break;
		case 4:
			cout << "Result a / b = " << one / two << endl << endl;
			break;
		default: break;
		}
	}

	return 0;
}

 
