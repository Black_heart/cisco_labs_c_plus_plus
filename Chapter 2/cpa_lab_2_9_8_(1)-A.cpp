﻿#include <iostream>

using namespace std;

int main()
{
	int one[10] = { 4, 7, 2, 8, 1, 3, 0, 9, 1, 5 };
	int two[10];
	int one_len = sizeof(one) / sizeof(*one);
	int two_len = sizeof(two) / sizeof(*two);

	two[0] = one[one_len - 1];
	for (size_t i = 1; i < one_len && i < two_len; ++i)
	{
		two[i] = one[i - 1];
	}
	for (size_t i = 0; i < two_len; ++i)
	{
		cout << two[i] << ' ';
	}
	cout << endl;

	return 0;
}


   
