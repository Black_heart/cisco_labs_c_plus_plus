#include <iostream>
#include <vector>

using namespace std;

class ip_address
{
protected:
        string ip;
public:
    ip_address(const ip_address &ip)
    {
        this->ip = ip.ip;
    };
    ip_address(string ip)
    {
        this->ip = ip;
    };
    void virtual print()
    {
        cout << this->ip << endl;
    }
};

class checked_ip_address : ip_address
{
public:
    checked_ip_address(const checked_ip_address &ip) : ip_address(ip)
    {
        this->correct = ip.correct;
    }
    checked_ip_address(string ip) : ip_address(ip)
    {
        correct = validate(ip);
    }
    void print()
    {
        cout << this->ip << (correct ? " - Correct" : " - Not Correct") << endl;
    }
private:
    bool correct;

    int validate(string ip_addr)
    {
        int ipSection[4] = {0, 0, 0, 0};
        char c = '\0';
        if(sscanf(ip_addr.c_str(), "%d.%d.%d.%d%s", &ipSection[0], &ipSection[1], &ipSection[2], &ipSection[3], &c) >= 4 && c == '\0')
        {
            for(size_t i = 0; i < 4; ++i)
            {
                if(ipSection[i] > 255 || ipSection[i] < 0)
                {
                    return 0;
                }
            }
            return 1;
        }
        else
        {
            return 0;
        }
    }
};

int main()
{
	string s_ip, s_c_ip, s_c_ip_2;

	cin >> s_ip;
	cin >> s_c_ip;
	cin >> s_c_ip_2;

	ip_address ip = ip_address(s_ip);
	checked_ip_address cip = checked_ip_address(s_c_ip);
	checked_ip_address cip2 = checked_ip_address(s_c_ip_2);

    cout << endl;
	ip.print();
	cip.print();
	cip2.print();

	return 0;
}
