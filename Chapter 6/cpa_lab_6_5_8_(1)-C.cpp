#include <iostream>
#include <stdexcept>
#include <vector>
#include <sstream>

using namespace std;

class ip_address{
private:
    string ipAddr;
    bool validate_ip_addr(string ipAddr);
public:
    ip_address() {};
    ip_address(string ipAddr);
    string get_p_addr();
};

string ip_address::get_p_addr(){
    return this->ipAddr;
}


ip_address::ip_address(string ipAddress)
{
    if(!this->validate_ip_addr(ipAddress))
    {
        throw invalid_argument("Not valid IP address");
    }
    this->ipAddr = ipAddress;
    cout << ipAddr <<endl;
}

bool ip_address::validate_ip_addr(string ipAddr) {
    int ipSection[4] = {0, 0, 0, 0};
    char c = '\0';
    if(sscanf(ipAddr.c_str(), "%d.%d.%d.%d%s", &ipSection[0], &ipSection[1], &ipSection[2], &ipSection[3], &c) >= 4 && c == '\0')
    {
        for(int i = 0; i < 4; ++i)
        {
            if(ipSection[i] > 255 || ipSection[i] < 0)
            {
                return 0;
            }
        }
        return 1;
    }
    else
    {
        return 0;
    }
};

class network{
private:
    vector<ip_address> addresses;
    string name;
public:
    network(string name);
    void add_address(ip_address address);
    void print();
};

network::network(string name)
{
    this->name = name;
}

void network::add_address(ip_address address)
{
    this->addresses.push_back(address);
}

void network::print()
{
    cout << this->name << ":" << endl;
    for(int i = 0; i < this->addresses.size(); ++i)
    {
        cout << this->addresses[i].get_p_addr() << endl;
    }
}

int main()
{
    string address;
    ip_address ipAddress;
    network network1 = network("network 1"),
            network2 = network("network 2");

    for(size_t i = 1; i < 6; ++i)
    {
        cout << "Enter IP address: " << i << ":" << endl;
        cin >> address;
        try{
            ipAddress = ip_address(address);
            if(i <= 3)
                network1.add_address(ipAddress);
            if(i >= 3)
                network2.add_address(ipAddress);
        }
        catch (const exception& e){
            cout << e.what() << "Not valid." << endl;
            --i;
        }
    }
    cout << endl;
    network1.print();
    network2.print();
    return 0;
}
