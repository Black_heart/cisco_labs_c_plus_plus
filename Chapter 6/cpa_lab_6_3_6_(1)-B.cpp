#include <iostream>

using namespace std;

class figure
{
public:
    figure(char letter, int num) {
        this->letter = letter;
        this->num = num;
    }
    virtual bool ValidateMovement(char letter, int num) {
        return false;
    }
protected:
    char letter;
    int num;
};

class man : public figure
{
public:
    man(char letter, int num) :figure(letter, num) { }
    bool ValidateMovement(char letter, int num) {
        return (letter - this->letter == num - this->num == 1 || 
                letter - this->letter == -1 && num - this->num == 1) &&
                letter >= 'a' && letter <= 'h' && num > 0 && num <= 8;
    }
};

class king :public figure
{
public:
    king(char letter, int num) :figure(letter, num) { }
    bool ValidateMovement(char letter, int num) {
        return this->letter - letter == this->num - num && 
        letter >= 'a' && letter <= 'h' && num > 0 && num <= 8;
    }
};

int main()
{
	figure *piece;
    string pice_type;
    char new_letter;
    int new_num;

    cout << "Enter the type of piece[man|king]:";
    cin >> pice_type;
    
    if(pice_type == "man")
    {
        piece = new man('b', 1);
    }
    else if(pice_type == "king")
    {
        piece = new king('b',1);
    }
    else return 1;

	cout << "Enter new position for " << pice_type << " on b1 [letter][number]:";
	cin >> new_letter >> new_num;
	cout << (piece->ValidateMovement(new_letter, new_num) ? "true" : "false") << endl;

	return 0;
}


   
