#include <iostream>

using namespace std;

class base_draw{
public:
    virtual void draw() {
        cout << " /\\" << endl;
    }
};

class draw_1: public base_draw  {
public:
    void draw() {
        cout << "Drawing 1:" << endl;
        cout << " /\\" << endl;
        cout << "//\\\\" << endl;
    }
};

class draw_2: public base_draw {
public:
    void draw() {
        cout << "Drawing 2:" << endl;
        cout << " /\\" << endl;
        cout << "/**\\" << endl;
    }
};

class draw_3: public base_draw { 
public:
    void draw() {
        cout << "Drawing 2:" << endl;
        cout << " /\\" << endl;
        cout << "/++\\" << endl;
    }
};

int main()
{
    base_draw *draws[3];
    draws[0] = new draw_1();
    draws[1] = new draw_2();
    draws[2] = new draw_3();

    for(size_t i = 0; i < 3; ++i)
    {
        draws[i]->draw();
    }

    return 0;
}
