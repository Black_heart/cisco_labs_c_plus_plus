#include <iostream>

using namespace std;

class str_validator
{
    public:
        virtual ~str_validator() {};
        virtual bool is_valid(string input) = 0;
};

class exact_validator : public str_validator
{
private:
    string exactString;
public:
    exact_validator(string str);
    virtual bool is_valid(string input);
};

exact_validator::exact_validator(string str) : exactString(str) {}

bool exact_validator::is_valid(string input)
{
    return input == this->exactString;
}

class dummy_validator : public str_validator
{
    public:
        virtual bool is_valid(string input);
};

bool dummy_validator::is_valid(string input)
{
    return true;
}

void print_valid(str_validator &validator, string input)
{
    cout << "The string '" << input << "' is " << (validator.is_valid(input) ? "valid" : "invalid") << endl;
}

int main()
{
    dummy_validator dummy;
    print_valid(dummy, "hello");
    cout << endl;
    exact_validator exact("secret");
    print_valid(exact, "hello");
    print_valid(exact, "secret");
    return 0;
}


   
