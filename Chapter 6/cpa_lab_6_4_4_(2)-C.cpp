#include <iostream>
#include <regex>

using namespace std;

class string_validator
{
    public:
        virtual ~string_validator() {};
        virtual bool is_valid(string in) = 0;
};

class min_len_validator: public string_validator
{
public:
    min_len_validator(int length)
    {
        this->length = length;
    }
    bool is_valid(string in)
    {
        return in.length() > length;
    }
private:
    int length;
};

class max_len_validator: public string_validator
{
public:
    max_len_validator(int length)
    {
        this->length = length;
    }
    bool is_valid(string in)
    {
        return in.length() < length;
    }
private:
    int length;
};

class pattern_validator: public string_validator
{
public:
    pattern_validator(string pattern)
    {
        replace(pattern, "D", "[0-9]");
        replace(pattern, "A", "\\w");
        replace(pattern, "?", ".");
        this -> pattern = pattern;
    }
    void replace(string pattern, string symb, string regsymb)
    {
        int shift = 0;
        int index;
        do
        {
            pattern.replace(index,1,regsymb);
            shift=index;
            index = pattern.find(symb,shift);
        }
        while(index != -1);
    }
    bool is_valid(string in)
    {
        regex rgx(pattern);
        return regex_search(in, rgx);
    }
private:
    string pattern;
};

void print_valid(string_validator &v, string in)
{
    cout << "The string '" << in << "' is "
    << (v.is_valid(in) ? "valid" : "invalid") <<endl;
}

int main()
{
    cout << "MinLength Validator" << endl;
    min_len_validator min(6);
    print_valid(min, "hello");
    print_valid(min, "welcome");
    cout << endl;

    cout << "MaxLength Validator" << endl;
    max_len_validator max(6);
    print_valid(max, "hello");
    print_valid(max, "welcome");
    cout << endl;

    cout << "Pattern Validator" << endl;
    pattern_validator digit("D");
    print_valid(digit, "there are 2 types of sentences in the world");
    print_valid(digit, "valid and invalid ones");
    cout << endl;

    return 0;
}


  
