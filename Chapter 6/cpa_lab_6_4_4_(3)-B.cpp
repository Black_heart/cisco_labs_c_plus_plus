#include <iostream>
#include <regex>

using namespace std;

class string_validator
{
public:
    virtual ~string_validator() {};
    virtual bool is_valid(string input) {
        return false;
    }
};

class min_len_validator :public string_validator {
private:
    int min_len;
public:
    min_len_validator(int min_len) {
        this->min_len = min_len;
    }
    bool is_valid(string input) {
        return input.length() >= min_len;
    }
};

class man_len_validator :public string_validator {
private:
    int max_len;
public:
    man_len_validator(int max) {
        max_len = max;
    }
    bool is_valid(string input) {
        return input.length() <= max_len;
    }
};

class pattern_validator :public string_validator {
private:
    int max;
    regex *digit;
public:
    pattern_validator(string i) {
        i = regex_replace(i, *(new regex("D")), string("\\d"));

        i = regex_replace(i, *(new regex("[a-ce-z]")), string("[a-ce-zB-Z]"));
        i = regex_replace(i, *(new regex("A")), string("[a-zA-Z]"));

        i = regex_replace(i, *(new regex("\\?")), string("."));
        i = regex_replace(i, *(new regex("[^\\?\\s\\d\\w\\\\]")), string("\\[^\\?\\s\\d\\w\\\\]"));
        digit = new regex(i);
    }
    bool is_valid(string input) {
        return regex_match(input, *digit);
    }
};

class password_validator : public string_validator
{
private:
    regex *digit, *lower, *upper, *special;

public:
    password_validator() {
        digit = new regex(".*\\d+.*");
        lower = new regex(".*[a-z]+.*");
        upper = new regex(".*[A-Z]+.*");
        special = new regex(".*\\W+.*");
    }
    bool is_valid(string input) {
        return input.length() >= 8 && regex_match(input, *digit) &&
            regex_match(input, *lower) && regex_match(input, *upper) && regex_match(input, *special);
    }
};

void print_valid(string_validator *validator, string input)
{
	cout << "The string '" << input << "' is "
		<< (validator->is_valid(input) ? "valid" : "invalid") << endl;
}


int main()
{
	cout << "MinLength Validator" << endl;
	min_len_validator min(6);
	print_valid(&min, "hello");
	print_valid(&min, "welcome");
	cout << endl;

	cout << "MaxLength Validator" << endl;
	man_len_validator max(6);
	print_valid(&max, "hello");
	print_valid(&max, "welcome");
	cout << endl;

	cout << "Pattern Validator" << endl << endl << "Digit Validation:" << endl;
	pattern_validator digit("D");
	print_valid(&digit, "2");
	print_valid(&digit, "valid and invalid ones");
	cout << endl << "Password Validation:" << endl;
	password_validator we;
	print_valid(&we, "2");
	print_valid(&we, "qwWED34//");
	cout << endl;
	return 0;
}


   
