#include <iostream>

using namespace std;

class engine
{
public:
    engine(string capacity = "1.0") {this->capacity = capacity;}
    void print()
    {
        cout << "engine: " << capacity << endl;
    }
private:
    string capacity;
};

class wheel
{
public:
    wheel(int diameter = 16) {this->diameter = diameter;}
    void print()
    {
        cout << "wheel: " << diameter <<"inches" << endl;
    }
private:
    int diameter;
};

class classic
{
public:
    classic(string type = "Normal") {this->type = type;}
    void print()
    {
        cout << "classic: " << type << endl;
    }
private:
    string type;
};

class lights
{
public:
    lights() {}
    void set_type(string type) {this->type = type;}
    void print()
    {
        cout << "Light: " << type << endl;
    }
private:
    string type;
};

class body
{
public:
    body(string color = "Black") {this->color = color;}
    void print()
    {
        cout << "body: " << color << endl;
    }
private:
    string color;
};

class car
{
    public:
        car()
        {
            l0.set_type("Type 1");
            l1.set_type("Type 1");
            l2.set_type("Type 2");
            l3.set_type("Type 2");
            l4.set_type("Type 3");
        }
        void print()
        {
            engine.print();
            wh1.print();
            wh2.print();
            wh3.print();
            wh4.print();
            chassis.print();
            l0.print();
            l1.print();
            l2.print();
            l3.print();
            l4.print();
            body.print();
        }
    private:
        engine engine;
        wheel wh1, wh2, wh3, wh4;
        classic chassis;
        lights l0,l1, l2,l3,l4;
        body body;
};

int main()
{
    car car;
    car.print();
    return 0;
}
